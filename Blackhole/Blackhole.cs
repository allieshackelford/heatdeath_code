﻿// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blackhole : MonoBehaviour
{
    [SerializeField] private readonly bool _Menu;
    public static List<Blackhole> Blackholes = new List<Blackhole>();
    public enum Colors{
        white,
        blue,
        green,
        orange,
        pink
    }

    // cached values
    [SerializeField] private Blackhole _Blackhole;
    private Rigidbody2D _Rigidbody;
    private Animator _Anim;
    public Vector3 Scale;  // get local scale
    private PointEffector2D _Effector; // get parent point effector
    private BlackholeAudio _Audio;
    
    // cool down and merge variables
    private bool _CanMerge = true;
    private readonly float _CoolDown = 0.8f;
    private float _NextTime;
    private bool _Created;
    public Colors Color;
    public static bool _GameEnding;
    public bool _ChosenOne;
    private bool _hasEnded;
    public bool MergeSpawn;

    // for menus
    public bool _Disabled;
    public bool _NoSound;

    private void OnEnable() {
        if(Blackholes.Contains(this)) return;
        Blackholes.Add(this);    
    }

    private void OnDisable() {
        if(Blackholes.Contains(this) == false) return;
        Blackholes.Remove(this);
    }

    private void Start()
    {
        Color = Colors.white;
        // reset local scale for lerping
        Scale = transform.localScale;
        _Effector = GetComponent<PointEffector2D>();
        _Rigidbody = GetComponent<Rigidbody2D>();
        _Anim = GetComponent<Animator>();
        _Audio = GetComponent<BlackholeAudio>();
        if(!MergeSpawn || !_NoSound) _Audio.PlaySpawn();
    }

    private void Update()
    {
        if(_Disabled) return;
        if(_hasEnded) {
            Leaderboard._LeaderboadActivator = true;
            DestroySelf();
        }
        if (_GameEnding && !_ChosenOne) DestroySelf();
        if (_GameEnding) return;
        GrowOverTime();
    }

    public void GameOverState()
    {
        _GameEnding = true;
        FindObjectOfType<CameraShake>().Shake(.5f,2,1);
        StartCoroutine(EndGameGrow());
    }

    private IEnumerator EndGameGrow()
    {
        bool growing = true;
        Vector3 newScale = transform.localScale * 5f;
        _Audio.PlayCritical();
        yield return new WaitForSeconds(1f);
        FindObjectOfType<CameraShake>().Shake(1.5f,4,2);
        while (growing)
        {
            transform.localScale = Vector3.Lerp(transform.localScale, newScale, 0.005f * Time.time);
            if (transform.localScale.x >= newScale.x - .10) growing = false;
            yield return null;
        }
        yield return new WaitForSeconds(1f);
        _hasEnded = true;
    }

    private void GrowOverTime(){
        // slowly increase local scale over time
        Scale += new Vector3(.0005f, .0005f, .0005f);
        transform.localScale = Scale;
        // slowly increase gravity of blackhole
        _Effector.forceMagnitude = _Effector.forceMagnitude - .05f;
    }

    public void BlackholeMerge(Collider2D collision){
        FindObjectOfType<CameraShake>().Shake(0.5f,1,1);
        FindObjectOfType<PostProc>().MergeProc();
        // if smaller, return and let larger blackhole handle merge
        Transform other = collision.gameObject.transform.parent;
        if(transform.localScale.x < other.transform.localScale.x) return;
        // checker so two blackholes aren't created when they merge
        if(_CanMerge == false) return;
        _CanMerge = false;
        collision.GetComponentInParent<Blackhole>()._CanMerge = false;
        _Audio.PlayMerge();
        // create new blackhole where two merged
        Vector3 middlePos = (transform.position + collision.transform.position) / 2;
        Blackhole hole = Instantiate(_Blackhole, middlePos, transform.rotation) as Blackhole;
        hole.MergeSpawn = true;
        Vector3 tempScale = Scale + other.transform.localScale;
        // update scale to be the size of both blackholes / 1.5 (small loss in size)
        hole.transform.localScale = new Vector3(tempScale.x / 1f, tempScale.y / 1f, tempScale.z / 1f);
        // blackhole destroys self and other
        DestroySelf();
        //Destroy(gameObject);
        //Destroy(collision.transform.parent.gameObject);
        other.gameObject.GetComponent<Blackhole>().DestroySelf();
    }

    public void DestroySelf()
    {
        Destroy(gameObject);
    }

    public void BulletGrowth(Collider2D collision){
        if (Time.time > _NextTime)
        {
            // reset cooldown counter
            _NextTime = Time.time + _CoolDown;
            // increase blackhole localScale
            Scale += new Vector3(.3f, .3f, .3f);
            transform.localScale = Scale;

            // change blackhole point effector
            _Effector.forceMagnitude = _Effector.forceMagnitude - 5;
            _Rigidbody.mass += 5;
        }
        // destroy bullet
        Destroy(collision.gameObject);
    }

    public void SetAnim(Collider2D collision){
        switch(collision.gameObject.layer){
            case 9: // blue
                _Anim.SetTrigger("BlueHit");
                Color = Colors.blue;
                break;
            case 12:    // green
                Color = Colors.green;
                _Anim.SetTrigger("GreenHit");
                break;
            case 13:   // orange 
                Color = Colors.orange;
                _Anim.SetTrigger("OrangeHit");
                break;
            case 14:   // pink
                Color = Colors.pink;
                _Anim.SetTrigger("PinkHit");
                break;
        }
    }
}
