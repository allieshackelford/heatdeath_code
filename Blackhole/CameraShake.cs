﻿// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 

using System.Collections;
using Cinemachine;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    private CinemachineVirtualCamera _Camera;
    private CinemachineBasicMultiChannelPerlin _Noise;
    private bool _IsShaking;

    private void Start() {
        _Camera = GetComponent<CinemachineVirtualCamera>();
        _Noise = _Camera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
    }

    private void Noise(float amp, float freq) {
        _Noise.m_AmplitudeGain = amp;
        _Noise.m_FrequencyGain = freq;    
    }

    public void Shake(float timeShake, float amp, float freq){
       if(!_IsShaking) StartCoroutine(Shaking(timeShake, amp, freq));
    }

    private IEnumerator Shaking(float duration, float amp, float freq){
        _IsShaking = true;
        Noise(amp,freq);
        yield return new WaitForSeconds(duration);
        Noise(0,0);
        _IsShaking = false;
    }

}
