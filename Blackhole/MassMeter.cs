﻿// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 
using UnityEngine;
using UnityEngine.UI;

public class MassMeter : MonoBehaviour
{
    [SerializeField] private Image _Meter;
    [SerializeField] private Animator _BarAnim;
    private bool _First;


    private void FindLargest(){
        float size = 0;
        foreach(var blackhole in Blackhole.Blackholes){
            if(blackhole.Scale.x >= 7 && !Leaderboard._Ended)
            {
                Leaderboard._Ended = true;
                FindObjectOfType<RandomMusicGenerator>().StopMusic();
                blackhole._ChosenOne = true;
                blackhole.GameOverState();
                return;
            }
            if (blackhole.Scale.x > size){
                size = blackhole.Scale.x;
            }
            
        }

        SetMeter(size);
    }

    private void SetMeter(float size)
    {
        if(size == 0) return;
        float percentage = (size / 7);

        _Meter.fillAmount = percentage;

        if(percentage >= .85 && !_First){
            _First = true;
            _BarAnim.SetTrigger("Flash");
        }
            
    }

    private void Update() {
        FindLargest();
    }
}
