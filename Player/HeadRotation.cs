﻿// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadRotation : MonoBehaviour
{
    [SerializeField] private DeadHead _Head;
    [SerializeField] private Sprite _BrokenHead;
    private SpriteRenderer _HeadSprite;
    public Texture2D SpriteSheet;
    public Texture2D DeadHeadSheet;
    public int[] Indexes;
    private Sprite[] _Heads;
    private Sprite[] _DeadHeads;
    public int[] DeadHeadIndexes;
    private int _LastIndex;
    

    private void Start()
    {
        _HeadSprite = GetComponentInChildren<SpriteRenderer>();
        _Heads = Resources.LoadAll<Sprite>(SpriteSheet.name);
        _DeadHeads = Resources.LoadAll<Sprite>(DeadHeadSheet.name);
        _LastIndex = DeadHeadIndexes[3];
    }

    public void SetHead(int index)
    {
        switch (index) {
            case 0:
                _HeadSprite.sprite = _Heads[Indexes[0]];
                _LastIndex = DeadHeadIndexes[0];
                break;
            case 1:
                _HeadSprite.sprite = _Heads[Indexes[1]];
                _LastIndex = DeadHeadIndexes[1];
                break;
            case 2:
                _HeadSprite.sprite = _Heads[Indexes[2]];
                _LastIndex = DeadHeadIndexes[2];
                break;
            case 3:
                _HeadSprite.sprite = _Heads[Indexes[3]];
                _LastIndex = DeadHeadIndexes[3];
                break;
            case 4:
                _HeadSprite.sprite = _Heads[Indexes[4]];
                _LastIndex = DeadHeadIndexes[4];
                break;
            case 5:
                _HeadSprite.sprite = _Heads[Indexes[5]];
                _LastIndex = DeadHeadIndexes[5];
                break;
            case 6:
                _HeadSprite.sprite = _Heads[Indexes[6]];
                _LastIndex = DeadHeadIndexes[6];
                break;
            case 7:
                _HeadSprite.sprite = _Heads[Indexes[7]];
                _LastIndex = DeadHeadIndexes[6];
                break;
        }
    }

    public void CreateDeadHead()
    {
        gameObject.SetActive(false);
        Instantiate(_Head, transform.position, transform.rotation)
                .SetHead(_DeadHeads[_LastIndex], _BrokenHead);
    }
}
