﻿// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunController : MonoBehaviour
{
    public enum PlayerColors{
        blue, 
        green,
        orange,
        pink
    }
    [SerializeField] private string _Rhorizontal;
    [SerializeField] private string _Rvertical;
    [SerializeField] private string _ShootButton;
    [SerializeField] private string _AltShootButton;
    [SerializeField] private Transform _ShootPoint;
    [SerializeField] private Bullet _Bullet;
    // cached gameobjects
    [SerializeField] private Weapon _DefaultGun;
    [SerializeField] private Weapon _AlternateGun;
    [SerializeField] private SpriteRenderer _Crosshair;
    [SerializeField] private PlayerColors _Color;
    [SerializeField] private SpriteRenderer _GunRend;
    private PlayerAudio _Audio;

    // input helpers
    private float _DeadZone = .25f;
    private float _PrevHoriz = 0;
    private float _PrevVert = 0;
    public bool IsGhost;
    private float _AnglePlayer;
    private float _GunTimer = 0;

    private float _NextShot;
    private float _NextAltShot;

    private void Start() {
        _Audio = GetComponentInParent<PlayerAudio>();
        if(_Crosshair != null) _Crosshair.enabled = false;
        SetGunSprite(_DefaultGun);
    }

    private void Update() {
        if (Leaderboard._Ended || MenuController.IsPaused) return;
        Rotate();
        Shoot();
        if(_GunTimer > 0) {
            _GunTimer--;
            if(_GunTimer <= 0) SetGunSprite(_DefaultGun);
        }
    }

    private void Rotate(){
        _PrevHoriz = Input.GetAxis(_Rhorizontal);
        _PrevVert = Input.GetAxis(_Rvertical);

        Vector2 deadZoneCheck = new Vector2(_PrevHoriz, _PrevVert);
        if(deadZoneCheck.magnitude < _DeadZone)
        {
            if(_Crosshair != null) _Crosshair.enabled = false;
            return;
        }

        _AnglePlayer = Mathf.Atan2(-_PrevVert, _PrevHoriz) * Mathf.Rad2Deg;
        // flip gun sprite if player looking left
        if(_AnglePlayer < -91 || _AnglePlayer > 91) {
            if(_GunRend != null) _GunRend.flipY = true;
        }
        else {
            if(_GunRend != null) _GunRend.flipY = false;
        }

        gameObject.transform.parent.transform.GetChild(0).GetComponent<AnimController>().SetAnimRotation(_AnglePlayer);
        transform.rotation = Quaternion.Euler(0, 0, _AnglePlayer);
        if(_Crosshair != null)_Crosshair.enabled = true;
    }

    // shooting
    private void Shoot()
    {
        // shoot if not ghost
        if ((Input.GetButtonDown(_ShootButton) || (Input.GetAxis(_AltShootButton) > .25 && Time.time > _NextAltShot)) && IsGhost == false && Time.time > _NextShot)
        {
            
            _Audio.PlayGun();
            if(_AlternateGun != null && _GunTimer > 0)
            {
                _NextShot = Time.time + _AlternateGun.CoolDown;
                _NextAltShot = Time.time + _AlternateGun.AltCoolDown;
                _AlternateGun.Shoot(_ShootPoint, _Bullet, this);
                GetComponentInParent<Rigidbody2D>().AddForce(-transform.right * _AlternateGun.PushBack, ForceMode2D.Impulse);
                return;
            }
            _NextShot = Time.time + _DefaultGun.CoolDown;
            _NextAltShot = Time.time + _DefaultGun.AltCoolDown;
            _DefaultGun.Shoot(_ShootPoint, _Bullet, this);
            GetComponentInParent<Rigidbody2D>().AddForce(-transform.right * _DefaultGun.PushBack, ForceMode2D.Impulse);
        }
    }

    public void SetGun(Weapon newGun){
        
        SetGunSprite(newGun);
        _Audio.PlayPickUp();
        _AlternateGun = newGun;
        _GunTimer = 700f;
    }

    private void SetGunSprite(Weapon setGun){
        if(_GunRend == null) return;
        switch(_Color){
            case PlayerColors.blue:
                _GunRend.sprite = setGun.blueSprite;
                break;
            case PlayerColors.green:
                _GunRend.sprite = setGun.greenSprite;
                break;
            case PlayerColors.orange:
                _GunRend.sprite = setGun.orangeSprite;
                break;
            case PlayerColors.pink:
                _GunRend.sprite = setGun.pinkSprite;
                break;
        }
    }
}
