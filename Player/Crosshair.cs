﻿// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crosshair : MonoBehaviour
{
    [SerializeField]
    private string _HorizontalAxis = "RHorizontal_P1";

    [SerializeField]
    private string _VerticalAxis = "RVertical_P1";

    [SerializeField]
    private float _Radius;

    private float _HorizontalInput = 1f;
    private float _VerticalInput = 0f;
    private Vector2 prevDir = Vector2.right;
    private Vector2 newDir = Vector2.right;

    private void Update()
    {
        _HorizontalInput = Input.GetAxis("RHorizontal_P1");
        _VerticalInput = Input.GetAxis("RVertical_P1");
        prevDir = newDir;

        newDir = new Vector2(_HorizontalInput,- _VerticalInput);
        newDir.Normalize();

        if (newDir.magnitude <= 0.05f)
        {
            newDir = prevDir;
        }
        else
        {
            prevDir = newDir;
        }
        transform.localPosition = newDir * _Radius;
    }
}
