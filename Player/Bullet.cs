﻿// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 

using UnityEngine;

public class Bullet : MonoBehaviour
{
    // gameobject to be instantiated
    [SerializeField] private GameObject _Blackhole;
    private Rigidbody2D _Rigidbody;
    public bool IsCreated;
    public int PlayerID;

    private void Start()
    {
        // get rigidbody and apply impulse force 
        _Rigidbody = GetComponent<Rigidbody2D>();
        _Rigidbody.AddForce(gameObject.transform.right * 15f, ForceMode2D.Impulse);
        Destroy(gameObject, 4f);
    }

    private void Update()
    { 
        // check change in direction and rotate towards that direction
        float angle = Mathf.Atan2(_Rigidbody.velocity.y, _Rigidbody.velocity.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(CheckCollision(collision)){
            // checker to make sure two blackholes aren't created
            if (!collision.gameObject.GetComponent<Bullet>().IsCreated)
            {
                // create blackhole at collision position
                Instantiate(_Blackhole, transform.position, transform.rotation);
                IsCreated = true;
            }
            // destroy self

            Destroy(gameObject);
        }

        // check if collided with player
        else if(collision.gameObject.layer == 8){
            if(collision.gameObject.GetComponent<CharacterController>()._Dying){
                Destroy(gameObject);
                return;
            }
            if (CheckIfSelf(collision))
            {
                Leaderboard.Instance._Players[PlayerID].AddSelf();
                Destroy(gameObject);
                return;
            }
            Leaderboard.Instance._Players[PlayerID].AddKill();
            Destroy(gameObject);
        }

        else if(collision.gameObject.tag == "Unbreakable"){
            collision.gameObject.GetComponent<Unbreakable>().SetAnimation();
        }
        
    }

    private bool CheckIfSelf(Collision2D collision){
        switch(PlayerID){
            case 0:
                if(collision.gameObject.tag == "BluePlayer"){
                    return true;
                }
                break;
            case 1:
                if(collision.gameObject.tag == "GreenPlayer"){
                    return true;
                }
                break;
            case 2:
                if(collision.gameObject.tag == "OrangePlayer"){
                    return true;
                }
                break;
            case 3:
                if(collision.gameObject.tag == "PinkPlayer"){
                    return true;
                }
                break;
            default:
                return false;
        }

        return false;
    }

    private bool CheckCollision(Collision2D collision){
        if(collision.gameObject.tag == "Bullet" ||
            collision.gameObject.tag == "Bullet2" ||
            collision.gameObject.tag == "Bullet3" ||
            collision.gameObject.tag == "Bullet4"){
                return true;
            }

        return false;
    }
}
