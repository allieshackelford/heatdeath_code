﻿// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimController : MonoBehaviour
{
    private Animator _Anim;

    private void Start()
    {
        _Anim = GetComponent<Animator>();
    }

    public void SetAnimRotation(float angle)
    {
        float horiz = 0;
        float vert = 0;
        int headIndex = 0;
        if (angle <= 22 && angle >= -22)
        { // right
            horiz = 1;
            vert = 0;
            headIndex = 5;
        }
        else if (angle >= 22.5 && angle <= 67)
        { // upright
            horiz = 1;
            vert = 1;
            headIndex = 6;
        }
        else if (angle >= 67.5 && angle <= 112)
        { // up
            horiz = 0;
            vert = 1;
            headIndex = 7;
        }
        else if (angle >= 112.5 && angle <= 157)
        { // upleft
            horiz = -1;
            vert = 1;
            headIndex = 0;
        }
        else if ((angle >= 157 && angle <= 180) || (angle >= -180 && angle <= -157.5))
        { // left
            horiz = -1;
            vert = 0;
            headIndex = 1;
        }
        else if (angle >= -157 && angle <= -112.5)
        {  // down left
            horiz = -1;
            vert = -1;
            headIndex = 2;
        }
        else if (angle >= -112 && angle <= -67.5)
        {   // down
            horiz = 0;
            vert = -1;
            headIndex = 3;
        }
        else if (angle >= -67 && angle <= -22.5)
        {    // down right
            horiz = 1;
            vert = -1;
            headIndex = 4;
        }
        _Anim.SetFloat("Horizontal", horiz);
        _Anim.SetFloat("Vertical", vert);
        GetComponentInChildren<HeadRotation>().SetHead(headIndex);
    }
}
