﻿// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour
{
    private SpriteRenderer _Renderer;
    private Rigidbody2D _Rigidbody;
    private Animator _Anim;
    private PlayerAudio _Audio;
    //private ParticleSystem _DustParticles;

    // multiplayer axis input
    [SerializeField] private string _Lhorizontal;
    [SerializeField] private string _Lvertical;
    [SerializeField]private float _MoveSpeed = 0.1f;
    public bool _Dying;
    private float _Invulnerable = 0f;
    public int PlayerID;

    [SerializeField] private GameObject _GhostPlayer;

    private void Start()
    {
        _Rigidbody = GetComponent<Rigidbody2D>();
        _Renderer = GetComponent<SpriteRenderer>();
        _Anim = GetComponentInChildren<Animator>();
        _Audio = GetComponent<PlayerAudio>();
        if(!Leaderboard._Ended) _Audio.PlaySpawn();
        //_DustParticles = GetComponentInChildren<ParticleSystem>();
    }

    private void Update() {
        if (Leaderboard._Ended) return;
        if(_Invulnerable > 0){
            _Invulnerable--;
            if(_Invulnerable == 0){
                gameObject.layer = 8;
            }
        }
    }

    private void FixedUpdate() {
        if (Leaderboard._Ended || MenuController.IsPaused) return;
        if (_Dying) return;
        Move();
    }

    // movement
    private void Move()
    {
        Vector2 deadZoneCheck = new Vector2(Input.GetAxis(_Lhorizontal), Input.GetAxis(_Lvertical));
        if (deadZoneCheck.magnitude < .15)
        {
            _Rigidbody.velocity = new Vector2(0, 0);
            _Anim.SetFloat("Velocity", 0);
            //_DustParticles.Stop();
            return;
        }
        _Rigidbody.velocity = new Vector2(Input.GetAxis(_Lhorizontal), -Input.GetAxis(_Lvertical)) * _MoveSpeed;
        if(_Rigidbody.velocity.magnitude > 0.2)
        {
            _Anim.SetFloat("Velocity", 1);
            //_DustParticles.Play();
        }
    }

    public void Respawn(){
        gameObject.layer = 18;
        _Invulnerable = 100;
    }

    private IEnumerator Die(){
        _Dying = true;
        Leaderboard.Instance._Players[PlayerID].AddDeath();
        _Rigidbody.velocity = new Vector2(0, 0);
        _Audio.PlayDeath();
        _Anim.SetTrigger("Death");
        yield return new WaitForSeconds(.5f);
        _Anim.ResetTrigger("Death");
        GetComponentInChildren<HeadRotation>().CreateDeadHead();
        yield return new WaitForSeconds(.3f);
        Instantiate(_GhostPlayer, transform.position, transform.rotation);
        Destroy(gameObject);
    }

    private void OnCollisionEnter2D(Collision2D other) {
        if(Leaderboard._Ended) return;
        // copy recent bullet 4 times and rename like provided
        if(other.gameObject.tag == "Bullet"){
            if (!_Dying) StartCoroutine(Die());  
        }
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if(Leaderboard._Ended) return;
        if(other.gameObject.tag == "Blackhole"){
            if(!_Dying) StartCoroutine(Die());
        }
    }

}
