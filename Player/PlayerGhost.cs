﻿// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGhost : MonoBehaviour
{
    private Rigidbody2D _Rigidbody;
    private Animator _Anim;

    // multiplayer axis input
    [SerializeField] private string _Lhorizontal;
    [SerializeField] private string _Lvertical;

    [SerializeField] private float _MoveSpeed = 80f;

    [SerializeField] private GameObject _Player;
    private float _GhostTimer;

    private void Start()
    {
        _Rigidbody = GetComponent<Rigidbody2D>();
        _Anim = GetComponentInChildren<Animator>();
        _GhostTimer = 300f; 
    }

    private void Update()
    {
        UpdateGhost();
    }

    private void UpdateGhost() {
        _GhostTimer--;
        if(_GhostTimer <= 0)
        {
            GameObject newPlayer = Instantiate(_Player, transform.position, transform.rotation) as GameObject;
            newPlayer.GetComponent<CharacterController>().Respawn();
            Destroy(gameObject);
        }
    }

    private void FixedUpdate()
    {
        Move();
    }

    // movement
    private void Move()
    {
        Vector2 deadZoneCheck = new Vector2(Input.GetAxis(_Lhorizontal), Input.GetAxis(_Lvertical));
        if (deadZoneCheck.magnitude < .15)
        {
            _Rigidbody.velocity = new Vector2(0,0);
            _Anim.SetFloat("Velocity", 0);
            return;
        }
        _Rigidbody.velocity = new Vector2(Input.GetAxis(_Lhorizontal), -Input.GetAxis(_Lvertical)) * _MoveSpeed;
        if(_Rigidbody.velocity.magnitude > 0.2)
        {
            _Anim.SetFloat("Velocity", 1);
        }
    }
}
