﻿// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 

using UnityEngine;

[CreateAssetMenu(menuName = "LeaderboardCell")]
public class LeaderboardCell : ScriptableObject
{
    public Sprite _PlayerImage;
    public int _Kills;
    public int _Deaths;
    public int _Self;
    public int _PlayerID;
    public Color _WinColor;
    public Color _LoseColor;

    public void AddKill()
    {
        _Kills++;
    }

    public void AddDeath()
    {
        _Deaths++;
    }

    public void AddSelf()
    {
        _Self++;
    }

    public void AddAll(LeaderboardCell cell)
    {
        _Kills += cell._Kills;
        _Deaths += cell._Deaths;
        _Self += cell._Self;
    }

    public void Reset(){
        _Kills = 0;
        _Deaths = 0;
        _Self = 0;
    }
}
