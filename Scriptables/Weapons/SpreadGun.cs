﻿// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 

using System.Collections;
using UnityEngine;

[CreateAssetMenu(menuName = "Weapons/SpreadGun")]
public class SpreadGun : Weapon
{
    public override void Shoot(Transform gun, Bullet bullet, GunController gunController)
    {
        gunController.StartCoroutine(ThreeShot(gun, bullet));
    }

    private IEnumerator ThreeShot(Transform gun, Bullet bullet)
    {
        for (int i = 0; i < 3; i++)
        {
            Instantiate(bullet, gun.position, gun.rotation * Quaternion.Euler(0, 0, Random.Range(-15, 15)));
            yield return new WaitForSeconds(0.05f);
        }
    }

}
