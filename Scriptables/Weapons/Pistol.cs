﻿// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Weapons/Pistol")]
public class Pistol : Weapon
{
    public override void Shoot(Transform gun, Bullet bullet, GunController gunController)
    {
        Instantiate(bullet, gun.position, gun.rotation);
    }
}
