﻿// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Weapons/Weirdgun")]
public class WeirdGun : Weapon
{
    public override void Shoot(Transform gun, Bullet bullet, GunController gunController)
    {
        Instantiate(bullet, gun.position, gun.rotation * Quaternion.Euler(0,0,-45));
        Instantiate(bullet, gun.position - gun.transform.right * 2, gun.rotation * Quaternion.Euler(0,0,180 - gun.rotation.z));
        Instantiate(bullet, gun.position, gun.rotation * Quaternion.Euler(0, 0, 45));
    }
}
