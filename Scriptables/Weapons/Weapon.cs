﻿// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 

using UnityEngine;

public abstract class Weapon : ScriptableObject
{ 
    public Sprite plainSprite;
    public Sprite blueSprite;
    public Sprite greenSprite;
    public Sprite orangeSprite;
    public Sprite pinkSprite;
    public float PushBack;
    public float CoolDown;
    public float AltCoolDown;
    public abstract void Shoot(Transform gun, Bullet  bullet, GunController gunController);
}
