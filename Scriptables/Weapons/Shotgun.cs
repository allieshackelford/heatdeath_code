﻿// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Weapons/Shotgun")]
public class Shotgun : Weapon
{
    // spreads 3 bullets
    public override void Shoot(Transform gun, Bullet bullet, GunController gunController)
    {
        Instantiate(bullet, gun.position, gun.rotation * Quaternion.Euler(0,0,-35));
        Instantiate(bullet, gun.position, gun.rotation);
        Instantiate(bullet, gun.position, gun.rotation * Quaternion.Euler(0, 0, 35));
    }
}
