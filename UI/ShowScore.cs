﻿// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 

using UnityEngine;
using TMPro;

public class ShowScore : MonoBehaviour
{
    public int _PlayerIndex;
    [SerializeField] private TextMeshProUGUI _Score;

    private void Start() { 
        _Score.text = Leaderboard.Instance._Players[_PlayerIndex]._Kills.ToString();
    }

    private void Update() {
        _Score.text = Leaderboard.Instance._Players[_PlayerIndex]._Kills.ToString();
    }
}
