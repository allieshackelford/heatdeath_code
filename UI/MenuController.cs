﻿// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class MenuController : MonoBehaviour
{
    [SerializeField] private Canvas _PauseCanvas;
    [SerializeField] private Canvas _UIScores;
    [SerializeField] private TextMeshProUGUI _RoundNum;
    private int _SoundController = 0; // 0 means sound on, 1 means sound is closed
    private int _Randomer;
    public int _Rounds = 2;
    public static bool IsPaused;
    public static bool ChangedBack;

// ========= MAIN MENU ==========================
    public void OnStart(){
        Debug.Log("Start");
        if(RoundManager.Instance == null){
            Debug.Log("null");
        }
        Debug.Log(RoundManager.Instance);
        Debug.Log(GameSceneManager.Instance);
        RoundManager.Instance.SetRounds(_Rounds);
        GameSceneManager.Instance.LoadingScene();
    }

    public void OnExit()
    {
        Application.Quit();
    }
// =============OPTIONS MENU=====================
    public void OnSound()
    {
        if (_SoundController > 1)
        {
            _SoundController = 0;
        }

        else {
            _SoundController++;
        }
    }

    public void OnCredits()
    {
        SceneManager.LoadScene("Credits");
    }

// =========PAUSE MENU===================================

     public void OnContinue(){
        _PauseCanvas.gameObject.SetActive(false);
        Time.timeScale = 1f;
        IsPaused = false;
        ChangedBack = true;
    }

    public void OnBackMenu(){
        Leaderboard._LeaderboadActivator = false;
        Time.timeScale = 1f;
        SceneManager.LoadScene("Start");
        IsPaused = false;
    }

    private void OnPause()//Pause game
    {
        if (Input.GetButton("Pause"))
        {
            _PauseCanvas.gameObject.SetActive(true);
            Time.timeScale = 0;
            IsPaused = true;
        }
    }

    private void Update() {
        if(_PauseCanvas != null){
            OnPause();
        }

        if(Leaderboard._LeaderboadActivator){
            _UIScores.gameObject.SetActive(false);
        }
    }

    // ========ROUNDS OPTION=============
    private void Start()
    {
        if(_RoundNum != null)
        {
            _RoundNum.text = _Rounds.ToString();
        }
    }

    public void OnMoreRounds()
    {
        _Rounds++;
        if(_Rounds >= 7){
            _Rounds = 1;
        }
        _RoundNum.text = _Rounds.ToString();
    }


    //  =========LOADING SCENE ===================
    public void StartGame(){
        GameSceneManager.Instance.NextScene();
    }
}
