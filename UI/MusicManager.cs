﻿// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    [SerializeField] private AudioSource _LoopSource;
    [SerializeField] private AudioSource _Source;
    private bool _FirstBlackhole = true;

    private void Update()
    {
        if(Blackhole.Blackholes.Count > 0 && _FirstBlackhole){
            PlayBlackholeLoop();
            _FirstBlackhole = false;
        }
    }

    public void PlayBlackholeLoop(){
        _LoopSource.Play();
    }

    public void StopBlackholeLoop(){
        _LoopSource.Stop();
    }
}
