﻿// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 

using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    private int sceneIndex;

    private void Start()
    { 
        sceneIndex = SceneManager.GetActiveScene().buildIndex;
        Time.timeScale = 1;
    }
    public void LoadNextLevel()
    {
        sceneIndex++;
        SceneManager.LoadScene(sceneIndex);
    }
}
