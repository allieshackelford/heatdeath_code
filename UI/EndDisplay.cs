﻿// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EndDisplay : MonoBehaviour
{
    [SerializeField] private Sprite[] _Images;
    [SerializeField] private Image _DisplayImage;
    [SerializeField] private SpawnedHeads[] _Heads;

    private void Awake() {
        LeaderboardCell cell = RoundManager.Instance.ReportWinner();
        _DisplayImage.sprite = _Images[cell._PlayerID-1];
        HeadSpawner.Instance.AssignHead(_Heads[cell._PlayerID-1]);
    }

    private void Update() {
        if (Input.GetButtonDown("UI_Submit"))
        {
            RoundManager.Instance.ResetFinalScores();
            SceneManager.LoadScene("Start");
        }
    }
}
