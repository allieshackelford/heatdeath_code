﻿// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 
using UnityEngine;

public class SpriteHover : MonoBehaviour
{
    private float _Amp = -.12f;
    private float _Freq = 1f;

    private Vector3 _PosOffset;

    private void Start() {
        _PosOffset = transform.position;
    }

    private void Update() {
        Vector3 tempPos = _PosOffset;
        tempPos.y += Mathf.Sin(Time.fixedTime*Mathf.PI*_Freq) * _Amp;
 
        transform.position = tempPos;
    }
    
}
