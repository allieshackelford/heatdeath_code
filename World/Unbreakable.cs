﻿// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 
using System.Collections;
using UnityEngine;

public class Unbreakable : MonoBehaviour
{
    private Animator _Anim;
    private bool _Animating;

    private void Start() {
        _Anim = GetComponent<Animator>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        // TODO: figure out why this doesn't always work
        if(collision.gameObject.tag == "Blackhole")
        {
            Destroy(gameObject);
        }

    }

    public void SetAnimation(){
        if(_Animating) return;
        StartCoroutine(HitAnimation());
    }

    private IEnumerator HitAnimation(){
        _Animating = true;
        _Anim.SetTrigger("Hit");

        yield return new WaitForSeconds(.07f);

        _Anim.SetTrigger("Reset");
        _Animating = false;
    }
}
