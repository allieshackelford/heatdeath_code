﻿// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 
using System.Collections;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class PostProc : MonoBehaviour
{
    private PostProcessVolume _Volume;
    [SerializeField] private PostProcessProfile _Original;
    [SerializeField] private PostProcessProfile _OtherProfile;
    private bool _Changing;

    private void Start() {
        _Volume = GetComponent<PostProcessVolume>();
    }

    private void Update() {
        if(MenuController.IsPaused){
            _Volume.profile = _OtherProfile;
        }

        if(MenuController.ChangedBack){
            _Volume.profile = _Original;
            MenuController.ChangedBack = false;
        }
        
    }

    public void MergeProc(){
        if(_Changing) return;
        StartCoroutine(LerpChromAberr());
    }

    private IEnumerator LerpChromAberr(){
        _Changing = true;
        // change to different profile
        _Volume.profile = _OtherProfile;

        yield return new WaitForSeconds(.6f);

        _Volume.profile = _Original;
        // change back to original
        _Changing = false;
    }
}
