﻿// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 

using UnityEngine;
using UnityEngine.SceneManagement;

public class GameSceneManager : Singleton<GameSceneManager>
{
    private int _Randomer;
    private string _LastLevel;
    private string[] _Levels = { "Level-1", "Level-2", "Level-3", "Level-4", "Level-5", "Level-6", "Level-7", "Level-8" };

    protected override void SingletonAwake(){}

    private void OnEnable() {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnDisable() {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    public void LoadEndScene()
    {
        SceneManager.LoadScene("End");
    }

    public void LoadingScene(){
        SceneManager.LoadScene("Loading");
    }

    public void NextScene() {
        Time.timeScale = 1;
        _Randomer = Random.Range(0, _Levels.Length);
        while(_Levels[_Randomer] == _LastLevel){
            _Randomer = Random.Range(0, _Levels.Length);
        }
        SceneManager.LoadScene(_Levels[_Randomer]);
        Blackhole._GameEnding = false;
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode){
        _LastLevel = scene.name;
        Leaderboard._LeaderboadActivator = false;
    }

   
}
