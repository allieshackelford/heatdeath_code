﻿// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 

using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Cell : MonoBehaviour
{
    [SerializeField] private Image _Cell;
    [SerializeField] private Image _PlayerImage;
    [SerializeField] private TextMeshProUGUI _Kills;
    [SerializeField] private TextMeshProUGUI _Deaths;
    [SerializeField] private TextMeshProUGUI _Self;

    private void Start() {
        _Cell = GetComponent<Image>();
    }

    private void SetElements(LeaderboardCell cell){
        _PlayerImage.sprite = cell._PlayerImage;
        _Kills.text = "KILLS: " + cell._Kills.ToString();
        _Deaths.text = "DEATHS: " + cell._Deaths.ToString();
        _Self.text = "SELF: " + cell._Self.ToString();
    }
    
    public void SetWinner(LeaderboardCell cell){
        SetElements(cell);
        _Cell.color = cell._WinColor;
    }

    public void SetLoser(LeaderboardCell cell){
        SetElements(cell);
        _Cell.color = cell._LoseColor;
    }
}
