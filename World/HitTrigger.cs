﻿// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitTrigger : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other) {
        if(Blackhole._GameEnding) return;
        if (other.gameObject.tag == "Blackhole")
        {
            GetComponentInParent<Blackhole>().BlackholeMerge(other);
        }
       
        else if (other.gameObject.tag == "Bullet")
        {
            if(CheckBulletColor(other)){
                GetComponentInParent<Blackhole>().BulletGrowth(other);
                GetComponentInParent<Blackhole>().SetAnim(other);
                return;
            }
            Destroy(other.gameObject);
        }

        else if(other.gameObject.tag == "Head"){
            if(CheckBulletColor(other)){
                GetComponentInParent<Blackhole>().SetAnim(other);
            }
            Destroy(other.gameObject);
        }
    }

    private bool CheckBulletColor(Collider2D bullet){
        switch(GetComponentInParent<Blackhole>().Color){
            case Blackhole.Colors.white: // white
                return true;
            case Blackhole.Colors.blue: // blue
                if(bullet.gameObject.layer != 9){
                    return true;
                }
                return false;
            case Blackhole.Colors.green: // green
                if(bullet.gameObject.layer != 12){
                    return true;
                }
                return false;
            case Blackhole.Colors.orange:   // orange
                if(bullet.gameObject.layer != 13){
                    return true;
                }
                return false;
            case Blackhole.Colors.pink: // pink
                if(bullet.gameObject.layer != 14){
                    return true;
                }
                return false;
            default:
                return false;
        }
    }
}
