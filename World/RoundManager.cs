﻿// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 

using System;
using UnityEngine;

public class RoundManager : Singleton<RoundManager>
{
    [SerializeField] private LeaderboardCell[] _FinalScores;
    // to monitor in inspector
    [SerializeField] private int _Rounds = 2;

    protected override void SingletonAwake(){
        DontDestroyOnLoad(gameObject);
    }

    public void SetRounds(int rounds)
    {
        _Rounds = rounds;
    }

    public void ReportScores(LeaderboardCell[] cells)
    {
        for(int i = 0; i < 4; i++){
            
            _FinalScores[i]._Kills += cells[i]._Kills;
        }
    }

    public void ResetFinalScores(){
        foreach(var item in _FinalScores){
            item.Reset();
        }
    }

    public LeaderboardCell ReportWinner(){
        LeaderboardCell[] _temp = new LeaderboardCell[4];
        Array.Copy(_FinalScores, _temp, 4);
        // sort players score 
        for(int i = 0; i < _temp.Length; i++){
            var cell = _temp[i];
            int j = i - 1;

            while(j >= 0 && _temp[j]._Kills < cell._Kills){
                _temp[j+1] = _temp[j];
                j--;
            }
            _temp[j+1] = cell;
        }

        return _temp[0];
    }

    public void NextRound()
    {
        _Rounds--;
        Time.timeScale = 1f;
        if(_Rounds <= 0)
        {
            GameSceneManager.Instance.LoadEndScene();
            return;
        }
        GameSceneManager.Instance.NextScene();
    }

   
}
