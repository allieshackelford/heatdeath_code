﻿// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Breakable : MonoBehaviour
{
    [SerializeField] private SpriteRenderer _ShadowSprite;
    private int _Health = 1;
    private bool _Animating;
    private Animator _Anim;
    private bool _Cooldown;

    private void Start() {
        _Anim = GetComponent<Animator>();
    }

    private void OnCollisionEnter2D(Collision2D other) {
        if(other.gameObject.tag == "Bullet" && !_Cooldown){
            Destroy(other.gameObject);
            _Cooldown = true;
            SetAnim();
            _Health++;
            if(_Health >= 4) {
                gameObject.GetComponent<Collider2D>().enabled = false;
                gameObject.GetComponent<SpriteRenderer>().sortingOrder = 1;
                _ShadowSprite.gameObject.SetActive(false);
            }
        }
        if(other.gameObject.tag == "Bullet"){
            Destroy(other.gameObject);
        }
    }

    private void SetAnim(){
        if(_Animating) return;
        StartCoroutine(Set());
    }

    private IEnumerator Set(){
        int curr = _Health;
        _Animating = true;
        _Anim.SetTrigger(curr.ToString());
        yield return new WaitForSeconds(.07f);
        _Anim.SetTrigger("White"+curr.ToString());
        _Animating = false;
        _Cooldown = false;
    }
}
