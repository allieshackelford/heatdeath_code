﻿// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 

using System;
using UnityEngine;

public class Leaderboard : Singleton<Leaderboard>
{
    [SerializeField] public LeaderboardCell[] _Players;
    [SerializeField] private Cell[] _UIElements;
    [SerializeField] private GameObject _Canvas;
    public static bool _LeaderboadActivator;
    private bool _IsFirst = true;
    public static bool _Ended;

    protected override void SingletonAwake()
    {
        _Canvas.gameObject.SetActive(false);
        _IsFirst = true;
    }

    private void Update()
    {
        if (Input.GetButtonDown("UI_Submit") && _LeaderboadActivator)
        {
            // reset cells
            RoundManager.Instance.ReportScores(_Players);
            ResetLevel();
            RoundManager.Instance.NextRound();
            _Ended = false;
        }
        if (_LeaderboadActivator && _IsFirst)
        {
            _IsFirst = false;
            UpdateLeaderboard();
            _Canvas.gameObject.SetActive(true);
            Time.timeScale = 0;
        }
    }

    private void ResetLevel()
    {
        ResetScores();
        _Canvas.gameObject.SetActive(false);
        Time.timeScale = 1f;
    }

    private void ResetScores(){
        foreach(var item in _Players){
            item.Reset();
        }
    }

    private void UpdateLeaderboard()
    {
        LeaderboardCell[] _temp = new LeaderboardCell[4];
        Array.Copy(_Players, _temp, 4);
        // sort players score 
        for(int i = 0; i < _temp.Length; i++){
            var cell = _temp[i];
            int j = i - 1;

            while(j >= 0 && _temp[j]._Kills < cell._Kills){
                _temp[j+1] = _temp[j];
                j--;
            }
            _temp[j+1] = cell;
        }

        // iterate cells and setelements
        for(int i = 0; i < _UIElements.Length; i++){
            if(i == 0){
                _UIElements[i].SetWinner(_temp[i]);
            }
            else{
                _UIElements[i].SetLoser(_temp[i]);
            }
        }
    }


}
