﻿// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 
using UnityEngine;

public class Boundary : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D other) {
        if(other.gameObject.tag == "Bullet"){
            Destroy(other.gameObject);
        }
    }
}
