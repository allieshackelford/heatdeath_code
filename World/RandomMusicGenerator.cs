﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomMusicGenerator : MonoBehaviour
{
    [SerializeField] private GameObject[] _Musics;
    [SerializeField] private AudioSource _EndGameMusic;
    private int _Index;

    private void Awake()
    {
        foreach (GameObject go in _Musics)
        {
            go.SetActive(false);
        }
    }

    private void Start()
    {
        _Index = Random.Range(0, _Musics.Length);

        StartMusic();
    }

    private void StartMusic(){
        _Musics[_Index].SetActive(true);
    }

    public void StopMusic(){
        _EndGameMusic.Play();
        _Musics[_Index].SetActive(false);
    }
}
