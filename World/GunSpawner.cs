﻿// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunSpawner : MonoBehaviour
{
    [SerializeField] private Weapon[] _Weapons;
    [SerializeField] private SpriteRenderer _GunSprite;
    private Animator _Anim;
    private int _Index;
    private bool _Ready = true;

    private void Start() {
        _Anim = GetComponent<Animator>();
        _Index = Random.Range(0, _Weapons.Length);   
        _GunSprite.sprite = _Weapons[_Index].plainSprite;
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if(_Ready){
            _GunSprite.sprite = null;
            _Ready = false;
            other.GetComponentInChildren<GunController>().SetGun(_Weapons[_Index]);
            _Index = Random.Range(0, _Weapons.Length);
            StartCoroutine(ResetSpawner());
        }
    }

    private IEnumerator ResetSpawner(){
        _Anim.SetTrigger("StepOn");
        yield return new WaitForSeconds(8.5f);
        _Anim.SetTrigger("Reignite");
        yield return new WaitForSeconds(.5f);
        _Anim.SetTrigger("ResetIdle");
        _Ready = true;
        _GunSprite.sprite = _Weapons[_Index].plainSprite;
    }
}
